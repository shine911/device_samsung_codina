CyanogenMod 12.0
=============================
Device Tree for Samsung Galaxy Ace 2
(GT-I8160)

How to build:
=============

- Make a workspace

  $ mkdir -p ~/cyanogenmod
  $ cd ~/cyanogenmod
  
- Do repo init & sync

  repo init -u https://github.com/CyanogenMod/android.git -b cm-12.0
  
  repo sync -j32

- Setup vendor
  
  ./vendor/cm/get-prebuilts
  
  . build/envsetup.sh
  
  . /device/samsung/codina/ste-ptaches.sh
		
- Build CM12.0
  
  brunch codina


- Thanks : CyanogenMod, dh-harald, Sakura Droid, jereksel, TeamCanjica, ekim.tecul, Meticulus
